module.exports = function(grunt) {
	grunt.initConfig({
		watch: {
			livereload: {
				options: {
				  livereload: 9090
				},
				files:['css/*.css', '*.html', 'js/*.js']
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['watch']);
};