(function($){
    'use strict';

    $.fn.queryFilter = function(option) {
        if (!this.length) {
            return;
        }
        var data = this.data('queryFilter'),
            options = (typeof option == 'object' && option) || {};
        if (!data) {
            this.data('queryFilter', new QueryFilter(this, options));
        }
        if (typeof option == 'string') {
            return data[option].apply(data, Array.prototype.slice.call(arguments, 1));
        }
        return this;
    };

    $.fn.queryFilter.constructor = QueryFilter;

    /**
     * Class QueryFilter
     */
    function QueryFilter(el, options) {

        var plugin = this,
			dataPoints = {},
			lastSerializeData;

		this.debug = true;

		this.event = {
			add: function(name, fn){
				el.on(name, fn);
			},
			fire: function(name){
				el.trigger(name);
			}
		}

        options = $.extend({
            dragDrop: false,
            dateFormat: "yy-mm-dd",
            draggableElement: '.draggble',
            droppableElement: '.group',
            valueSelector: ':input.js-value-field',
            initText: ''
        }, options);

        /**
         * Add new event
         * @param [string] name
         * @param [function] fn
         * @return void
		 */
        plugin.on = function(name, fn){
            this.event.add(name, fn);
        }

        /**
         * Bind plugin events
         * @return void
		 */
        function bindEvents(){
            el.on('click', '.group-nav .btn, .extend .btn', function(){
                var self = $(this),
                data = self.data('do');
                switch (data){
                    case 'add-ext':
                        addExt(self);
                        break
                    case 'add-group':
                        addGroup(self);
                        break
                    case 'delete-rule':
                        removeExt(self);
                        break
                    case 'delete-group':
                        removeGroup(self);
                        break
                }
                plugin.event.fire('change-value');
                plugin.dragDropInit();
            });

            el.on('blur change', options.valueSelector, function(e){
                if(!$(this).hasClass('time')){
                    plugin.event.fire('change-value');
                }
            });

            el.on('click', '.group-condition .btn', function(){
                var self = $(this);

                if (!self.is('.active')){
                    switchCondition(self);
                    plugin.event.fire('change-value');
                }
            });

            el.on('change', '.null-field input[type=checkbox]', function(){
                switchNullVal($(this));
                plugin.event.fire('change-value');
            });

            el.on('change', '.condition', function(){
                switchValue($(this));
            });

            el.on('keypress', '.value-wrap input.integer, .value-wrap input.float', function(e){
                var self = $(this),
                    val = self.val();

                // TODO: create new function (keypressValidation)
                if (self.is('.integer')){
                    if (e.which != 45 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        showLabel(self, 'Only integer', 1000);
                        return false;
                    }
                } else if (self.is('.float')){
                    if((e.which != 46 || self.val().indexOf('.') != -1) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))){
                        showLabel(self, 'Only float', 1000);
                        return false;
                    }
                }
            });

            plugin.on('change-value drop init', function(){
                var firstGroupBody = el.find('> .group .group-body');

                console.log(firstGroupBody.find('> div').length)

                if(firstGroupBody.find('> div:not(.init-text)').length == 0){
                    el.addClass('empty');
                    firstGroupBody.html('<h4 class="text-success text-center init-text">'+options.initText+'</h4>')
                } else {
                    el.removeClass('empty');
                    firstGroupBody.find('.init-text').remove();
                }
            });
        }

        /**
         * Unbind plugin events
         * @return void
		 */
        function unbindEvents(){
            el.off('click blur change keypress');
        }

        /**
         * ---------------------------------
         * Control functions
         * ---------------------------------
         */
        function addGroup(el){
            //el.closest('.group').data('index')
            //TODO: add current index
            el.closest('.group').children('.group-body').append(insertGroup('1'));
        }
        function removeExt(el){
            el.closest('.extend').remove();
        }
        function removeGroup(el){
            el.closest('.group').remove();
        }
        function switchCondition(el){
            el.addClass('active').siblings('.btn').removeClass('active');
        }
        function switchValue(el){
            var point = el.closest('.extend').data('point'),
                prevType = el.data('selected-type'),
                currentType = el.find(':selected').data('cond-type');

            if(prevType !== currentType){
                el.siblings('.value-wrap').html('');
                el.siblings('.value-wrap').append(insertValueField(el.val(), el.data('type'), dataPoints[point].options));
                el.data('selected-type', currentType);
            } else {
                plugin.event.fire('change-value');
            }

            if (el.val() == 'is any of' || el.val() == 'is none of'){
                textExtInit();
            }

            if (el.data('type') == 'date'){
                datepickerInit();
            } else if (el.data('type') == 'time') {
                inputMaskInit();
            } else if (el.data('type') == 'multi' || el.data('type') == 'single'){
                selectpickerInit();
            }
        }
        function switchNullVal(el){
            var expression = el.closest('.extend'),
                condition = expression.find('.condition').val(),
                valueField = expression.find('.value-wrap > *:input, .value-wrap .input-group > *:input');

            valueField.prop('disabled', el.is(':checked') && (condition == 'is' || condition == 'is not'));
            if(valueField.is('.selectpicker')){
                valueField.selectpicker('refresh');
            }
        }
        function removeFirstDelBtn(){
            el.children('.group').find('> .group-header .btn[data-do="delete-group"]').remove();
        }
        /**
         * Toggle helper message
         */
        function showLabel(el, text, delay, type){
            type = type || 'danger';
            var label = $("<span class='val-err label label-"+type+"'>"+text+"</span>");
            if (el.siblings('.label').length == 0){
                el.after(label);
                setTimeout(function(){
                    label.fadeOut('300', function(){
                        label.remove()
                    });
                }, delay);
            }
        }

        /**
         * Initialization
         * @return void
		 */
        function init(){
            el.addClass('query-filter');
			// init events
			plugin.event.add('render', function(){
                unbindEvents();
				bindEvents();
				plugin.dragDropInit();
				selectpickerInit();
				datepickerInit();
                textExtInit();
                inputMaskInit();
			});

			plugin.render(options.filters);
            removeFirstDelBtn();
            plugin.event.fire('init');
        }

        // template obj
        plugin.template = {};

        /**
         * Insert condition in expression
         */
        function insertExtendCondition(condition, type){
            condition = condition || '';
                // common
            var common ='\
                <select class="condition form-control" data-type="'+type+'" data-selected-type=""> \
                    <option data-cond-type="1">is</option> \
                    <option data-cond-type="1">is not</option>',
                // single,  multi, boolean
                type1 = '\
                    <option data-cond-type="2">is any of</option> \
                    <option data-cond-type="2">is none of</option> \
                </select>',
                // integer, float, date
                type2 = '\
                    <option data-cond-type="4">is greater than</option> \
                    <option data-cond-type="4">is less than</option> \
                    <option data-cond-type="4">is equal or greater than</option> \
                    <option data-cond-type="4">is equal or less than</option> \
                    <option data-cond-type="2">is any of</option> \
                    <option data-cond-type="2">is none of</option> \
                    <option data-cond-type="3">is between</option> \
                    <option data-cond-type="3">is not between</option> \
                </select>',
                // string
                type3 = '\
                    <option data-cond-type="5">contains</option> \
                    <option data-cond-type="5">does not contain</option> \
                    <option data-cond-type="5">starts with</option> \
                    <option data-cond-type="5">ends with</option> \
                    <option data-cond-type="5">does not start with</option> \
                    <option data-cond-type="5">does not end with</option> \
                </select>';

            var conditionStr;

            if (type == 'single' || type == 'multi' || type == 'boolean'){
                conditionStr = common + type1;
            } else if (type == 'integer' || type == 'float' || type == 'date' || type == 'time'){
                conditionStr = common + type2;
            } else if (type == 'string'){
                conditionStr = common + type3;
            }

            //convert str into obj and set active option
            var conditionObj = $(conditionStr);

            conditionObj.find('option').each(function(index){
                var self = $(this);

                if (self.text() == condition){
                    self.prop('selected', true)
                    conditionObj.data('selected-type', self.data('cond-type'));
                    return false;
                } else {
                    conditionObj.data('selected-type', 1)
                }
            });

            return conditionObj;
        }

        /**
        * Insert value in expression
        */
        function insertValueField(condition, type, options, value){

            var isNull = (value === null);
            condition = condition || '';
            value = value || '';

            var valueField;
            if (condition == 'is any of' || condition == 'is none of'){ // insert textarea
                //obj to arr
                var opts = [];
                $.each(options, function(index, val){
                    opts.push(val);
                });
                valueField = '<div class="value-wrap"><input class="textext form-control '+type+' '+(type == 'date'?' datepicker':'')+'" data-type-view="'+type+'" type="text" data-options="'+opts.join(',')+'" data-value="'+value+'">';
            } else if (condition == 'is between' || condition == 'is not between'){
                var valueFrom = value[0] !== undefined ? value[0] : '',
                    valueTo = value[1] !== undefined ? value[1] : '';
                if (type == 'date'){ //insert two date
                    valueField = '<div class="value-wrap">\
                        <div class="input-group"><input class="js-value-field form-control datepicker from" type="text" value="'+valueFrom+'"><span class="input-group-addon js__input-group"><span class="glyphicon glyphicon-calendar"></span></span></div> \
                        <i class="separator">-</i><div class="input-group"><input class="js-value-field form-control datepicker to" type="text" value="'+valueTo+'"><span class="input-group-addon js__input-group"><span class="glyphicon glyphicon-calendar"></span></span></div>';
                } else if (type == 'time') {
                    valueField = '<div class="value-wrap">\
                        <input class="js-value-field form-control '+type+'" type="text" value="'+valueFrom+'"><i class="separator">-</i><input class="js-value-field form-control '+type+'" type="text" value="'+valueTo+'">';
                } else { //insert two inputs
                    valueField = '<div class="value-wrap">\
                        <input class="js-value-field form-control '+type+'" type="text" value="'+valueFrom+'"><i class="separator">-</i><input class="js-value-field form-control '+type+'" type="text" value="'+valueTo+'">';
                }
            } else if (type == 'single' || type == 'multi'){ // insert select

                var optionsHtml = '';
                // if (!$.isArray(value)) {
                    // value = [value];
                // }

                $.each(options, function(index, val){
                    options[index]

                    var selected = index == value ? 'selected':'';
                    optionsHtml += '<option '+selected+' value='+index+'>'+this+'</option>';
                });

                valueField = '<div class="value-wrap">\
                    <select class="js-value-field selectpicker form-control '+type+'" data-live-search="true">'+optionsHtml+'</select>';
            } else if (type == 'integer' || type == 'float' || type == 'string'){ // insert input
                valueField = '<div class="value-wrap"><input class="js-value-field form-control '+type+'" type="text" value="'+value+'">';
            } else if (type == 'date'){ // insert datetimepicker
                valueField = '<div class="value-wrap"> \
                        <div class="input-group"><input class="js-value-field form-control datepicker" type="text" value="'+value+'"><span class="input-group-addon js__input-group"><span class="glyphicon glyphicon-calendar"></span></span></div>';
            } else if (type == 'time') { // insert select (yes,no)
                valueField = '<div class="value-wrap">\
                    <input class="js-value-field form-control '+type+'" type="text" value="'+value+'">';
            } else if (type == 'boolean') { // insert select (yes,no)
                valueField = '<div class="value-wrap"> \
                    <select class="js-value-field form-control">\
                        <option></option>\
                        <option>Yes</option>\
                        <option>No</option>\
                    </select>';
            }

            // insert checkbox and close wrapper
            if (condition == 'is' || condition == 'is not' || condition == 'is any of' || condition == 'is none of' || condition == ''){ // insert null and close div
                valueField = valueField + '<div class="checkbox null-field"><label><input type="checkbox" class="js-value-field"'+(isNull?' checked':'')+'>NULL</label></div></div>';
            } else { // close div
                valueField = valueField + '</div>';
            }

            return valueField;
        }

        /**
         * Insert expression in group
         */
        function insertExtend(index, point, obj){
            obj = obj || {};
			var data = dataPoints[point] || {};

            plugin.template.extend = '\
            <div class="extend" data-index="'+index+'" data-point="'+point+'"> \
                <span class="point text-success"><b>('+point+')</b> '+data.title+'</span> \
                <div class="extend-filter"></div> \
                <button class="btn btn-danger pull-right" data-do="delete-rule" type="button"><span class="glyphicon glyphicon-remove-sign"></span></button> \
            </div>';

            var extendObj = $(plugin.template.extend);
            var $filter = extendObj.find('.extend-filter');

            // insert condition
            $filter.append(
                insertExtendCondition(obj.condition, data.type, obj.value),
                insertValueField(obj.condition, data.type, data.options, obj.value)
            );

            switchNullVal($filter.find('.null-field input[type=checkbox]'));

            return extendObj;
        }

        /**
         * Insert condition in group
         */
        function insertGroupCondition(condition){

            condition = condition || '';

            plugin.template.groupConditionBtns = '<div class="group-condition btn-group btn-group-sm pull-left">\
                <button class="condition-or btn btn-primary" type="button">OR</button>\
                <button class="condition-and btn btn-primary" type="button">AND</button>\
            </div>';

            var groupCondition = $(plugin.template.groupConditionBtns);
            groupCondition.find(condition == 'or' ? '.condition-or' : '.condition-and').addClass('active');

            return groupCondition;
        }

        plugin.template.groupNav = '\
            <div class="group-nav btn-group btn-group-sm pull-right"> \
                <button class="btn btn-success" data-do="add-group" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Group</button> \
                <button class="btn btn-danger" data-do="delete-group" type="button"><span class="glyphicon glyphicon-remove-sign"></span> Remove</button> \
            </div>';

        /**
         * Create new grope
         */
        function insertGroup(index, obj){

			// trace('insert.group', index, obj);

            obj = obj || {};

            plugin.template.group = '\
                <div class="group" data-index="'+index+'"> \
                    <div class="group-header"> \
                        '+ plugin.template.groupNav  +' \
                    </div> \
                    <div class="group-body"></div> \
                </div>';

            var group = $(plugin.template.group);
            group.find('.group-header').prepend(insertGroupCondition(obj.operator));

            return group;
        }

		/**
		 * Debug tool
		 * @return void
		 */
		function trace(){
			if (plugin.debug && console){
				console.log.apply(console, ['[QueryFilter]', arguments]);
			}
		}

		/**
		 * Clear
		 * @return void
		 */
        function clear(){

			// trace('clear');

			dataPoints = {};
			el.html('');
		}

		/**
		 * Render
		 */
        plugin.render = function(data){

			// trace('render', data);
			clear();

			// parse json
			if (typeof data == 'string') {
				data = JSON.parse(data);
			}

			// prepare codes
			var codes = [];
			renderRecursive(data, function(row){
				if (row.point && $.inArray(row.point, codes) == -1) {
					codes.push(row.point);
				}
			});


			// load data points by codes
			loadData(codes, function(res){
				$.each(res, function(){
					dataPoints[this.code] = this;
				});
				data.indexes = [0];
				renderRecursive(data, function(item){
					renderRow(item);
				});
				plugin.event.fire('render');
                removeFirstDelBtn();
			});

            /**
            * Build interface (groups, expressions)
            */
            function renderRow(row){

				// trace('render.row', row.type, row.indexes.join('.'), row);

				var lastIndex = row.indexes.pop(),
                    parentIndex = row.indexes.join('.');
				row.indexes.push(lastIndex);
				var index = row.indexes.join('.');

                if (index == '0'){
                    el.append(insertGroup(index, row))
                    if(row.type == 'expression'){
                        el.find('.group[data-index="0"] .group-body').first().append(insertExtend(index, row.point, row));
                    }
                } else if (row.type == 'group') {
                    el.find('.group[data-index="'+parentIndex+'"] .group-body').first().append(insertGroup(index, row))
                } else if (row.type == 'expression') {
                    el.find('.group[data-index="'+parentIndex+'"] .group-body').first().append(insertExtend(index, row.point, row))
                }
            }
        }

		/**
		* Recursive function
		*/
        function renderRecursive(row, callback){

			// trace('render.recursive', row.type, row);

			if ($.isFunction(callback)){
				callback(row);
			}
			if (row.expressions) {
				$.each(row.expressions, function(index){
					this.indexes = row.indexes ? row.indexes.slice(0) : [];
					this.indexes.push(index);
					renderRecursive(this, callback);
				});
			}
        }

        /**
         * @return object
         */
		plugin.serialize = function(){
            var isValid = true, serializeData = serializeGroup(el);
            if (isValid || !lastSerializeData) {
                lastSerializeData = serializeData;
            }

			return lastSerializeData;

			/**
			 * @param [jQuery object] el
			 * @return object
			 */
			function serializeRow(el){
                var condition = el.find('.condition').val(),
                    res = {
                        type: 'expression',
                        point: el.data('point'),
                        condition: condition,
                        value: []
                    };

				el.find('.value-wrap').find(options.valueSelector).each(function(){
                    var self = $(this);
                    if (self.is(':checkbox')){
                        if (self.is(':checked')){
                            res.value.push('null');
                        }
                    } else if (self.is('.textext')){
                        res.value = $.parseJSON(self.val());
                    }
                    else {
                        res.value.push(self.val());
                    }
                });

                if ($.inArray(condition, ['is', 'is not'])>=0 && $.inArray('null', res.value)>=0) {
                    res.value = 'null';
                } else if (res.value.length==1){
                    if (condition !== 'is any of' && condition !== 'is none of'){
                        res.value = res.value[0];
                    }
				}

                if (!res.value || res.value.length==0 || (res.value.length==1 && res.value[0] == "")){
                    isValid = false;
                }

				return res;
			}

			/**
			 * @param [jQuery object] el
			 * @return object
			 */
			function serializeGroup(el){
                    var res = [],
                        body = el.find('.group-body').first();

                    if(body.children().length > 1){
                        res = {
                            type: 'group',
                            operator: el.find('.group-condition button.active').html(),
                            expressions: []
                        };
                    }
				body.children().each(function(){
					var el = $(this);

					if (el.hasClass('group')){
                        res.expressions ? res.expressions.push(serializeGroup(el)) : res = serializeGroup(el);
					} else {
                        res.expressions ?  res.expressions.push(serializeRow(el)) : res = serializeRow(el);
					}
				});
				return res;
			}
		}

        /**
         * @param [mixed] codes
         * @param [function] callback
         * @return void
         */
        function loadData(codes, successCallback, errorCallback){
            if($.type(options.resource) == 'string'){
                $.ajax({
                    url: options.resource,
                    method: 'POST',
                    type: 'json',
                    data: {codes: codes},
                    beforeSend: function(){
                        el.addClass('loading');
                    },
                    complete: function(){
                        el.removeClass('loading');
                    },
                    success: function(res){
                        if ($.isFunction(successCallback)){
                            successCallback(res);
                        }
                    },
                    error: function(xhr){
                        if ($.isFunction(errorCallback)){
                            errorCallback();
                        }
                    }
                });
            } else {
                successCallback(options.resource);
            }
        }

		/**
		 * UI drag and drop init
		 */
        plugin.dragDropInit = function(){
            if(options.dragDrop){
                $(options.draggableElement).draggable({
                    helper: "clone"
                });

                $(options.droppableElement).droppable({
                    greedy: true,
                    hoverClass: "group-hover",
                    drop: function(event, ui) {
                        var self = $(this),
                            code = ui.draggable.data('point'),
                            index = self.data('index');

                        if(dataPoints[code] === undefined){
                            loadData([code], function(obj){
                                dataPoints[code] = obj[code];
                                self.children('.group-body').append(insertExtend(index, code));
                                if (dataPoints[code].type == 'single' || dataPoints[code].type == 'multi'){
                                    selectpickerInit();
                                } else if (dataPoints[code].type == 'date'){
                                    datepickerInit();
                                } else if (dataPoints[code].type == 'time'){
                                    inputMaskInit();
                                }
                                textExtInit();
                                plugin.event.fire('drop');
                            });
                        } else {
                            self.children('.group-body').append(insertExtend(index, code));
                            if (dataPoints[code].type == 'single' || dataPoints[code].type == 'multi'){
                                selectpickerInit();
                            } else if (dataPoints[code].type == 'date'){
                                datepickerInit();
                            } else if (dataPoints[code].type == 'time'){
                                inputMaskInit();
                            }
                            textExtInit();
                            plugin.event.fire('drop');
                        }
                    }
                });
            }
        }

		/**
		* Selectpicker init
		*/
        function selectpickerInit(){
            el.find('.selectpicker').selectpicker();
        }

		/**
		 * UI datepicker init
		 */
        function datepickerInit(){
            var datepicker = $('.extend', el).find('.datepicker');

            datepicker.each(function(){
                var self = $(this),
                    pickerOptions = {
                        dateFormat: options.dateFormat,
                        onSelect: function(){
                            if (self.is('.textext')) {
                                self.textext()[0].tags().addTags([ self.val() ]);
                                self.val('');
                            }
                            plugin.event.fire('change-value');
                        }
                    };

                if (self.is('.from')){
                    $.extend(pickerOptions, {
                        numberOfMonths: 2,
                        onClose: function(selectedDate){
                            self.parents('.extend-filter').find('.datepicker.to').datepicker("option", "minDate", selectedDate);
                        }
                    });
                    self.datepicker(pickerOptions);
                } else if (self.is('.to')){
                    $.extend(pickerOptions, {
                        numberOfMonths: 2,
                        onClose: function(selectedDate){
                            self.parents('.extend-filter').find('.datepicker.from').datepicker("option", "maxDate", selectedDate);
                        }
                    });
                    self.datepicker(pickerOptions);
                } else {
                    self.datepicker(pickerOptions);
                }
            });

            //trigger icon
            el.on('click', '.input-group-addon.js__input-group', function(){
                $(this).siblings('input.form-control').focus();
            });
        }

        /**
         * textext init
         * TODO: change on select 2 or choosen
         */
        function textExtInit(){
            el.find('.textext').not('.inited').each(function(){
                var self = $(this),
                    data = self.data(),
                    dataTypeView = data['typeView'],
                    dataOptions = data['options'] ? data['options'].split(',') : null,
                    dataValue = data['value'] ? data['value'].split(',') : null;

                if (dataTypeView == 'multi' || dataTypeView == 'single' || dataTypeView == 'boolean'){ // multi, single field
                    self.textext({
                        plugins : 'tags arrow autocomplete filter',
                        filterItems: dataOptions,
                        tagsItems: dataValue,
                        html: {
                            tag: '<div class="text-tag label label-info"><div class="text-button"><span class="text-label"/><span class="text-remove glyphicon glyphicon-remove-sign"/></div></div>'
                        }
                    })
                    .on('keyup', function(e, data){
                        var textext = self.textext()[0],
                            query = self.val() ? self.val() : '';
                        self.trigger('setSuggestions', {result: textext.itemManager().filter(dataOptions, query)});
                    });

                    /* fix bug of textext plugin */
                    el.on('mouseup', '.text-wrap .text-list *',function(e){
                        var self = $(this);

                		if(self.hasClass('text-suggestion') || self.hasClass('text-label')){
                            self.trigger('click');
                        }
                    });

                    // trigger show droplist
                    self.on('focus', function(){
                        $(this).trigger('keyup');
                        $(this).siblings('.text-dropdown').show();
                    });

                } else { // only tags
                    self.textext({
                        plugins : 'tags',
                        tagsItems: dataValue,
                        html: {
                            tag: '<div class="text-tag label label-info"><div class="text-button"><span class="text-label"/><span class="text-remove glyphicon glyphicon-remove-sign"/></div></div>'
                        }
                    });

                    // add data tag
                    if (dataTypeView == 'date'){
                        self.on('change', function(e){
                            var self = $(this);
                            self.textext()[0].tags().addTags([''+self.val()+'']);
                            self.val('');
                        });
                    }

                }

                self.textext()[0].bind('setFormData', function(e, data){
                    plugin.event.fire('change-value');
                });

                // add class for correct serialize
                self.addClass('inited');
                self.siblings('input[type="hidden"]').addClass('js-value-field textext inited');
            });

        }

        /* jQuery input mask init */
        function inputMaskInit(){
            el.find('input.time, input[data-type-view="time"]').inputmask("hh:mm",{
                "placeholder": "ЧЧ:ММ",
                "clearIncomplete": true,
                "oncomplete": function(){
                    plugin.event.fire('change-value');
                }
            });
        }

		init();
    }

})(jQuery);
